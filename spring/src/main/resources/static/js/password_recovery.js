class PasswordRecovery {
	static async send (recovery_method, data) {
		if (recovery_method == 'sms') {
			return PasswordRecovery.sendSMS(data)
		}
		else if (recovery_method == 'email') {
			return PasswordRecovery.sendEmail(data)
		}
	}

	static async sendSMS (data) {
		const data_obj = {obj_type: 'PasswordRecovery_sendSMS', cellphone: data}

		const url = '/recover/sms/send'
		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data_obj)
		}
		const response_json = await fetch(url, options).then(response => {
			if (!response.ok) {
				throw new Error('Bad response', response)
			}

			return response.json()
		}).catch(error => {
			console.error(error)
			return {status: 'error'}
		})

		return response_json
	}

	static async sendEmail (data) {
		const data_obj = {obj_type: 'PasswordRecovery_sendEmail', email: data}

		const url = '/recover/email'
		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data_obj)
		}
		const response_json = await fetch(url, options).then(response => {
			if (!response.ok) {
				throw new Error('Bad response', response)
			}

			return response.json()
		}).catch(error => {
			console.error(error)
			return {status: 'error'}
		})

		return response_json
	}
}
