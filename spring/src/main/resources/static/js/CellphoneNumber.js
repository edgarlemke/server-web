class CellphoneNumber {
	constructor (value) {
		CellphoneNumber.validate(value)

		this.value = value
	}

	static validate (value) {
		throw new Error('CellphoneNumber.validate():  Invalid value: ' + value)
	}
}
