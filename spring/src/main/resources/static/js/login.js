class Login {
	static async send (username, password) {
		const data_obj = {obj_type: 'Login_send', username, password}

		const url = '/login'
		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data_obj)
		}
		const response_json = await fetch(url, options).then(response => {
			if (!response.ok) {
				throw new Error('Bad response', response)
			}

			return response.json()
		}).then(response_json => {
			if (!Object.keys(response_json).includes('status')) {
				throw new Error(`Response JSON doesn't have 'status' key`, response_json)
			}
			return response_json
		}).catch(error => {
			console.error(error)
			return {status: 'error'}
		})

		// Redirect to private home page if status is ok
		if (response_json.status == 'ok') {
			window.location.href = '/private?sid=' + response_json.sid
		}

		// Continuing only if there are errors to show
		return response_json
	}
}
