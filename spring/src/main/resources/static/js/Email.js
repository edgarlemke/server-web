class Email {
	constructor (value) {
		Email.validate(value)

		this.value = value
	}

	static validate (value) {
		throw new Exception('Email.validate():  Invalid value: ' + value)
	}
}
