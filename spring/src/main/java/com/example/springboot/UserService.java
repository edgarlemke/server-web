package com.example.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.springboot.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public boolean authenticateUser(String username, String password) {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			return false;
		}

		boolean match = bCryptPasswordEncoder.matches(password, user.getPasswordHash());

		return match;
	}
}
