package com.example.springboot;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

@Entity
public class Session {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	protected Session() {}

	public void Session () {
	}

	public Long getId () {
		return this.id;
	}
}
