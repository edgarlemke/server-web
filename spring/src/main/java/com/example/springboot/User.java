package com.example.springboot;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String username;
	private String password_hash;

	protected User() {}

	public User (String username) {
		this.username = username;
	}

	public Long getId () {
		return this.id;
	}

	public String getUsername () {
		return this.username;
	}

	public String getPasswordHash () {
		return this.password_hash;
	}
}
