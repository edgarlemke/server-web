package com.example.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.ui.Model;

import java.util.Map;

@Controller
public class PasswordRecoveryController {
	@GetMapping("/recover")
	public String recover(Model model) {
		model.addAttribute("title", "Recuperar senha");
		return "recover";
	}

	@PostMapping("/recover/sms/send")
	public ResponseEntity<String> recover_sms_send(@RequestBody Map<String, Object> payload) {
		String cellphone = (String) payload.get("cellphone");

		return new ResponseEntity<>(String.format("{\"obj_type\": \"PasswordRecovery_sendSMS_response\", \"status\": \"ok\", \"cellphone\": \"%s\"}", cellphone), HttpStatus.OK);
	}

	@PostMapping("/recover/email")
	public ResponseEntity<String> recover_email(@RequestBody Map<String, Object> payload) {
		String email = (String) payload.get("email");

		return new ResponseEntity<>(String.format("{\"obj_type\": \"PasswordRecovery_sendEmail_response\", \"status\": \"ok\", \"email\": \"%s\"}", email), HttpStatus.OK);
	}
}
