package com.example.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import com.example.springboot.SessionService;

@Controller
public class SessionController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private SessionService sessionService;
	

	@PostMapping("/login")
	public ResponseEntity<String> login(@RequestBody Map<String, Object> payload) {
		String username = (String) payload.get("username");
		String password = (String) payload.get("password");

		if (!userService.authenticateUser(username, password)) {
			return new ResponseEntity<>("{\"obj_type\": \"Login_send_response\", \"status\": \"invalid\"}", HttpStatus.UNAUTHORIZED);
		}

		String sid = sessionService.createSession(username);

		return new ResponseEntity<>(String.format("{\"obj_type\": \"Login_send_response\", \"status\": \"ok\", \"sid\": \"%s\"}", sid), HttpStatus.OK);
	}
}
