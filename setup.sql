CREATE SCHEMA bio;
CREATE USER 'bio'@'%' IDENTIFIED BY 'bio';
GRANT ALL PRIVILEGES ON bio.* TO 'bio'@'%';

USE bio;

CREATE TABLE user (
	id INT NOT NULL AUTO_INCREMENT,
	username TEXT NOT NULL,
	password_hash CHAR(72),
	PRIMARY KEY (id)
);

CREATE TABLE session (
	id CHAR,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (user_id) REFERENCES user(id)
);
